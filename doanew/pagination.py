from rest_framework import pagination


class LargeResultsSetPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class NonePaginationAtStartPagination(pagination.PageNumberPagination):
    page_size = 0
    page_size_query_param = 'page_size'
    max_page_size = 500


class ReturnAllPagination(pagination.PageNumberPagination):
    page_size = 0
    page_size_query_param = 'page_size'
    max_page_size = None

