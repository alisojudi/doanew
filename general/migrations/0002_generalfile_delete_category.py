# Generated by Django 4.2.4 on 2023-08-12 20:47

from django.db import migrations, models
import doanew.methods
import doanew.utils.thumbs


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='GeneralFile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('guid', models.CharField(blank=True, default='', max_length=40, unique=True)),
                ('image', doanew.utils.thumbs.ImageWithThumbsField(blank=True, null=True, upload_to=doanew.methods.update_filename, verbose_name='تصویر')),
                ('file', models.FileField(blank=True, null=True, upload_to=doanew.methods.update_filename, verbose_name='فایل')),
                ('position', models.PositiveSmallIntegerField(default=999, verbose_name='Position')),
                ('mime_type', models.CharField(blank=True, default='', max_length=40)),
                ('title', models.CharField(blank=True, default='', max_length=200)),
                ('description', models.TextField(blank=True, max_length=200)),
                ('insertion_datetime', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Insertion Date Time')),
            ],
            options={
                'verbose_name': 'فایل های بارگزاری شده',
                'verbose_name_plural': 'فایل های بارگزاری شده',
            },
        ),
        migrations.DeleteModel(
            name='Category',
        ),
    ]
