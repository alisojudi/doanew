from django.db import models
from general.models import GUIDModel, GeneralFile
from doanew.methods import _, __, update_filename
from doanew.utils.thumbs import ImageWithThumbsField
from ckeditor.fields import RichTextField
from django.utils.timezone import now




class BlogCategory(GUIDModel):
    is_active = models.BooleanField(verbose_name=_(u"Is Active"), blank=True, default=False)
    title = models.CharField(verbose_name=_(u"Title"), max_length=150, blank=True, default="")
    position = models.PositiveSmallIntegerField(verbose_name=_(u"Position"), default=999)

    class Meta:
        ordering = ('-position',)

    def __unicode__(self):
        return "%s" % (self.title, )

    def __str__(self):
        return "%s" % (self.title, )

    class Meta:
        verbose_name = _("دسته بندی وبلاگ ")
        verbose_name_plural = _(" دسته بندی وبلاگ")


class BlogTextFile(GUIDModel):
    image = ImageWithThumbsField(_(u"تصویر"), upload_to=update_filename, blank=True, null=True, sizes=((60, 60)))
    title = models.CharField(verbose_name=_(u"عنوان تصویر"), blank=True, max_length=700)

    def __unicode__(self):
        return str(self.title)

    def __str__(self):
        return str(self.title)


class Tag(GUIDModel):
    slug = models.CharField(verbose_name=_(u"عنوان "), blank=True, max_length=700)

    def __unicode__(self):
        return str(self.slug)

    def __str__(self):
        return str(self.slug)


class BlogPost(GUIDModel):
    auther = models.CharField(verbose_name=_(u"نویسنده"), blank=True, max_length=700)
    caption = models.CharField(verbose_name=_(u"عنوان"), blank=True, max_length=700)
    summary = models.TextField(verbose_name=_(u"خلاصه"), blank=True)
    view = models.IntegerField(_(u"بازدید  "), blank=True, default=0)
    content_image = models.ManyToManyField(BlogTextFile, default=None, blank=True, verbose_name=_("تصاویر محتوا"))
    content = RichTextField(verbose_name=_(u"محتوای پست"),)
    create_datetime = models.DateTimeField(verbose_name=_(u" تاریخ ایجاد  "), blank=True, default=now)
    image = models.ManyToManyField(GeneralFile, default=None, blank=True, verbose_name=_("تصویر"))

    tag = models.ManyToManyField(Tag, default=None, blank=True, verbose_name=_("تگ "))
    category = models.ForeignKey(BlogCategory, verbose_name=_(u"دسته بندی"), null=True, on_delete=models.CASCADE,
                                 related_name='category_blog', default=None)

    def __unicode__(self):
        return self.caption

    def __str__(self):
        return self.caption

    class Meta:
        verbose_name = _("مقالات وبلاگ")
        verbose_name_plural = _("مقالات وبلاگ")

