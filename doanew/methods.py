import os

MONTH_CHOICES = {1: 'فروردین', 2: 'اردیبهشت', 3: 'خرداد', 4: 'تیر', 5: 'مرداد', 6: 'شهریور', 7: 'مهر', 8: 'آبان',
                 9: 'آذر', 10: 'دی', 11: 'بهمن', 12: 'اسفند'}

def _(word):
    return  word

def __(word):
    return word



def update_filename(instance, filename):
    path = "entity_files/"
    filename, file_extension = os.path.splitext(filename)
    format = str(filename) + file_extension
    instance.title = filename
    return os.path.join(path, format)


