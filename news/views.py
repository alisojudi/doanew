from django.shortcuts import render
from doanew.pagination import ReturnAllPagination, NonePaginationAtStartPagination, LargeResultsSetPagination
from .models import BlogTextFile, BlogCategory, BlogPost
from rest_framework import generics
from .serializers import FileSerializer, BlogPostSerializer, BlogCategorySerializer
from django.db.models import Q


class BlogCategoryAPIView(generics.ListAPIView, generics.RetrieveAPIView):
    serializer_class = BlogCategorySerializer
    pagination_class = ReturnAllPagination

    def get_queryset(self):
        cats = BlogCategory.objects.all()
        return cats.order_by('-position')


class BlogPostAPIView(generics.ListAPIView, generics.RetrieveAPIView):
    serializer_class = BlogPostSerializer
    pagination_class = ReturnAllPagination

    def get_queryset(self):
        posts = BlogPost.objects.all()

        if 'category' in self.request.query_params.keys():
            posts = posts.filter(category__id=int(self.request.query_params['category']))

        if 'related' in self.request.query_params.keys():
            posts = posts.filter(guid=self.request.query_params['guid'])
            get_post = posts[0]
            get_category = get_post.category
            r_posts = BlogPost.objects.filter(Q(category=get_category) & ~Q(id__in=[get_post.id]))
            return r_posts.order_by('-create_datetime')

        if 'guid' in self.request.query_params.keys():
            posts = posts.filter(guid=self.request.query_params['guid'])
            posts[0].view += 1
            posts[0].save()

        if 'contains_text' in self.request.query_params.keys():
            posts = posts.filter(Q(caption__icontains=self.request.query_params['contains_text']) |
                                 Q(content__icontains=self.request.query_params['contains_text']))

        return posts.order_by('-create_datetime')

