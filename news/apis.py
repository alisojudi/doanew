from django.urls import path
from .views import BlogCategoryAPIView, BlogPostAPIView

urlpatterns = [
    path('blog_post', BlogPostAPIView.as_view(), name="blog_post"),
    path(r'^blog_category$', BlogCategoryAPIView.as_view(), name="blog_category"),
]