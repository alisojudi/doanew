# -*- encoding: utf-8 -*-
# Based on django-thumbs by Antonio Melé

# python imports
try:
    import Image
except ImportError:
    from PIL import Image

try:
    import cStringIO
except ImportError:
    from io import BytesIO as cStringIO

import os, re, math

# django imports
from django.core.files.base import ContentFile
from django.db.models import ImageField
from django.db.models.fields.files import ImageFieldFile
from django.conf import settings

# lfs imports
from doanew.utils.images import scale_to_max_size


def generate_thumb(img, thumb_size, format):
    """
    Generates a thumbnail image and returns a ContentFile object with the thumbnail

    Parameters:
    ===========
    img         File object

    thumb_size  desired thumbnail size, ie: (200,120)

    format      format of the original image ('jpeg','gif','png',...)
                (this format will be used for the generated thumbnail, too)
    """
    img.seek(0)
    image = Image.open(img)

    # Convert to RGB if necessary
    if image.mode not in ('L', 'RGB', 'RGBA'):
        image = image.convert('RGB')

    new_image = scale_to_max_size(image, *thumb_size)

    io = cStringIO.StringIO()

    # PNG and GIF are the same, JPG is JPEG, And TIF must be JPEG
    if format.upper() in ('JP2', 'JPE', 'JPG', 'TIF'):
        format = 'JPEG'

    new_image.save(io, format)
    return ContentFile(io.getvalue())


class ImageWithThumbsFieldFile(ImageFieldFile):
    """
    See ImageWithThumbsField for usage example
    """
    def __init__(self, *args, **kwargs):
        super(ImageWithThumbsFieldFile, self).__init__(*args, **kwargs)
        # self.sizes = settings.LFS_THUMBNAIL_SIZES
        #
        # if self.sizes:
        #     for size in self.sizes:
        #         (w, h) = size
        #         setattr(self, 'url_%sx%s' % (w, h), self._get_size_thumb_url(size))

    thumb_name_pattern = '%s.%sx%s.%s'

    def _get_size_thumb_url(self, size):
        (w, h) = size
        if not self:
            return ''
        else:
            split = self.url.rsplit('.', 1)
            thumb_url = self.thumb_name_pattern % (split[0], w, h, split[1])
            return thumb_url

    def _get_size_thumb_file(self, size):
        (w, h) = size
        if not self:
            return ''
        else:
            split = self.name.rsplit('.', 1)
            thumb_file_name = self.thumb_name_pattern % (split[0], w, h, split[1])
            return thumb_file_name

    def save(self, name, content, save=True):
        super(ImageWithThumbsFieldFile, self).save(name, content, save)
        # if self.sizes:
        #     for size in self.sizes:
        #         self.generate_thumb(size)

    def generate_thumb(self, size):
        (w, h) = size

        thumb_name = self._get_size_thumb_file(size)

        # you can use another thumbnailing function if you like
        split = self.name.rsplit('.', 1)
        thumb_content = generate_thumb(self.file, size, split[1])

        thumb_name_ = self.storage.save(thumb_name, thumb_content)

        setattr(self, 'url_%sx%s' % (w, h), self._get_size_thumb_url(size))

        if not thumb_name == thumb_name_:
            raise ValueError('There is already a file named %s' % thumb_name)

    def delete(self, save=True):
        name = self.name
        super(ImageWithThumbsFieldFile, self).delete(save)
        if self.sizes:
            for size in self.sizes:
                (w, h) = size
                split = name.rsplit('.', 1)
                thumb_name = '%s.%sx%s.%s' % (split[0], w, h, split[1])
                try:
                    self.storage.delete(thumb_name)
                except:
                    pass

    def __getattr__(self, item):
        url_pattern = "url_(\d+)x(\d+)"
        extracted_dimensions = re.search(url_pattern, item)
        if extracted_dimensions:
            size = (int(extracted_dimensions.group(1)), int(extracted_dimensions.group(2)))
            if not os.path.isfile(os.path.join(settings.MEDIA_ROOT, self._get_size_thumb_file(size))):
                self.generate_thumb(size)
            else:
                setattr(self, 'url_%sx%s' % size, self._get_size_thumb_url(size))

            return self._get_size_thumb_url(size)
        else:
            super(ImageWithThumbsFieldFile, self).__getattr__(item)


class ImageWithThumbsField(ImageField):
    attr_class = ImageWithThumbsFieldFile
    """
    Usage example:
    ==============
    photo = ImageWithThumbsField(upload_to='images', sizes=((125,125),(300,200),)

    To retrieve image URL, exactly the same way as with ImageField:
        my_object.photo.url
    To retrieve thumbnails URL's just add the size to it:
        my_object.photo.url_125x125
        my_object.photo.url_300x200

    Note: The 'sizes' attribute is not required. If you don't provide it,
    ImageWithThumbsField will act as a normal ImageField

    How it works:
    =============
    For each size in the 'sizes' atribute of the field it generates a
    thumbnail with that size and stores it following this format:

    available_filename.[width]x[height].extension

    Where 'available_filename' is the available filename returned by the storage
    backend for saving the original file.

    Following the usage example above: For storing a file called "photo.jpg" it saves:
    photo.jpg          (original file)
    photo.125x125.jpg  (first thumbnail)
    photo.300x200.jpg  (second thumbnail)

    With the default storage backend if photo.jpg already exists it will use these filenames:
    photo_.jpg
    photo_.125x125.jpg
    photo_.300x200.jpg

    Note: django-thumbs assumes that if filename "any_filename.jpg" is available
    filenames with this format "any_filename.[widht]x[height].jpg" will be available, too.

    To do:
    ======
    Add method to regenerate thubmnails

    """
    def __init__(self, verbose_name=None, name=None, width_field=None, height_field=None, sizes=None, **kwargs):
        super(ImageWithThumbsField, self).__init__(verbose_name=verbose_name,
                                                   name=name,
                                                   width_field=width_field,
                                                   height_field=height_field,
                                                   **kwargs)
        self.sizes = sizes

if 'south' in settings.INSTALLED_APPS:
    # south rules
    rules = [
      (
        (ImageWithThumbsField,),
        [],
        {
            "sizes": ["sizes", {"default": None}]
        },
      )
    ]
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules(rules, ["^lfs\.core\.fields\.thumbs"])
