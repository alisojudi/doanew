from jalali_date import datetime2jalali, date2jalali
from .models import BlogPost, BlogTextFile, GeneralFile, BlogCategory
import os, json, ast
import time, random
import jdatetime
import datetime
from rest_framework import serializers, pagination
from doanew.methods import MONTH_CHOICES
import uuid


class FileSerializer(serializers.ModelSerializer):

    class Meta:
        model = GeneralFile
        fields = '__all__'


class BlogCategorySerializer(serializers.ModelSerializer):
    post_count = serializers.SerializerMethodField()

    def get_post_count(self, obj):
        posts = BlogPost.objects.filter(category=obj)
        return posts.count()

    class Meta:
        model = BlogCategory
        fields = '__all__'


class BlogPostSerializer(serializers.ModelSerializer):
    image = FileSerializer(many=True, required=False)
    category = BlogCategorySerializer(required=False)
    month = serializers.SerializerMethodField()
    day = serializers.SerializerMethodField()
    year = serializers.SerializerMethodField()

    # age = serializers.SerializerMethodField()

    # def get_age(self, obj):
    #     date_time = obj.create_datetime
    #     now_epoch = datetime.datetime.now().timestamp()
    #     date_time_epoch = date_time.timestamp()
    #     t = int(now_epoch - date_time_epoch)
    #     return t

    def get_month(self, obj):
        get_datetime = obj.create_datetime
        jalali = datetime2jalali(get_datetime)

        return MONTH_CHOICES[jalali.month]

    def get_day(self, obj):
        get_datetime = obj.create_datetime
        jalali = datetime2jalali(get_datetime)

        return jalali.day

    def get_year(self, obj):
        get_datetime = obj.create_datetime
        jalali = datetime2jalali(get_datetime)

        return jalali.year

    class Meta:
        model = BlogPost
        fields = '__all__'