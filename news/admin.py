from django.contrib import admin
from news.models import BlogPost, BlogCategory, BlogTextFile
from django.utils.html import format_html, escape
from general.models import  GeneralFile
from doanew.settings import BASE_URL

class GeneralFileAdmin(admin.ModelAdmin):
    model = GeneralFile
    list_display = ('title', )
#     list_display = ('owner', 'is_block', 'is_confirm', 'is_delete', 'create_datetime')
#     list_editable = ('is_confirm',)
#     list_filter = ('is_confirm', 'is_reported', 'is_block', 'is_delete', )
#     search_fields = ['location__name', 'caption', 'owner__username', ]
#     readonly_fields = ('post_image_tag', )

#     def post_image_tag(self, obj):
#         set_images = ""
#         im = obj.image
#         set_images += '<a target="_blank" href="' + im.image.url + '" > <img style="display:inline-block;height:200px;" src="%s" /> </a>' % escape(im.image.url) + " "

#         return format_html(set_images)

#     post_image_tag.short_description = 'Post image  '
#     post_image_tag.allow_tags = True


admin.site.register(GeneralFile, GeneralFileAdmin)


class BlogTextFileAdmin(admin.ModelAdmin):
    model = BlogTextFile
    list_display = ('title', )
#     list_display = ('owner', 'is_block', 'is_confirm', 'is_delete', 'create_datetime')
#     list_editable = ('is_confirm',)
#     list_filter = ('is_confirm', 'is_reported', 'is_block', 'is_delete', )
#     search_fields = ['location__name', 'caption', 'owner__username', ]
#     readonly_fields = ('post_image_tag', )

#     def post_image_tag(self, obj):
#         set_images = ""
#         im = obj.image
#         set_images += '<a target="_blank" href="' + im.image.url + '" > <img style="display:inline-block;height:200px;" src="%s" /> </a>' % escape(im.image.url) + " "

#         return format_html(set_images)

#     post_image_tag.short_description = 'Post image  '
#     post_image_tag.allow_tags = True


admin.site.register(BlogTextFile, BlogTextFileAdmin)


class BlogCategoryCategoryAdmin(admin.ModelAdmin):
    model = BlogCategory
    exclude = ['guid', 'is_active', ]
    list_display = ('title', )
#     list_display = ('owner', 'is_block', 'is_confirm', 'is_delete', 'create_datetime')
#     list_editable = ('is_confirm',)
#     list_filter = ('is_confirm', 'is_reported', 'is_block', 'is_delete', )
#     search_fields = ['location__name', 'caption', 'owner__username', ]
#     readonly_fields = ('post_image_tag', )

#     def post_image_tag(self, obj):
#         set_images = ""
#         im = obj.image
#         set_images += '<a target="_blank" href="' + im.image.url + '" > <img style="display:inline-block;height:200px;" src="%s" /> </a>' % escape(im.image.url) + " "

#         return format_html(set_images)

#     post_image_tag.short_description = 'Post image  '
#     post_image_tag.allow_tags = True


admin.site.register(BlogCategory, BlogCategoryCategoryAdmin)


class BlogPostCategoryAdmin(admin.ModelAdmin):
    model = BlogPost
    exclude = ['guid', 'parent', ]
#     exclude = ['guid', ]
    list_display = ('caption', 'auther', 'category', )
#     list_editable = ('is_confirm',)
#     list_filter = ('is_confirm', 'is_reported', 'is_block', 'is_delete', )
#     search_fields = ['location__name', 'caption', 'owner__username', ]
    readonly_fields = ('post_image_tag', )

    def post_image_tag(self, obj):
        set_images = "<ul>"
        ims = obj.image.all()
        for im in ims:
            print(BASE_URL + im.image.url)
            set_images += '<li style="display:inline-block;width:100px;height:100px;" ><a style="display:block;width:100%;height:100%;"  target="_blank" href="' + BASE_URL + im.image.url + '" > <img  style="width: 100%;height: 100%;object-fit: cover;border-radius: 20px;" src="' + BASE_URL + im.image.url + '" /> </a></li>'

        set_images += "</ul>"
        return format_html(set_images)

    post_image_tag.short_description = ' نمایش تصویر  '
    post_image_tag.allow_tags = True


admin.site.register(BlogPost, BlogPostCategoryAdmin)
