from django.db import models
import os, hashlib, random, uuid, datetime
from doanew.utils.thumbs import ImageWithThumbsField
from doanew.methods import update_filename, _, __


class GUIDModel(models.Model):
    id = models.AutoField(primary_key=True,)
    guid = models.CharField(max_length=40, unique=True, blank=True, default="")

    def save(self, *args, **kwargs):
        if not self.guid:
            self.guid = uuid.uuid4()

        super(GUIDModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class SlugModel(models.Model):
    slug = models.SlugField(allow_unicode=True, unique=True, max_length=500, blank=True, default="")

    class Meta:
        abstract = True


class SlugGUIDModel(GUIDModel, SlugModel):

    class Meta:
        abstract = True


class GeneralFile(GUIDModel):
    image = ImageWithThumbsField(_(u"تصویر"), upload_to=update_filename, blank=True, null=True, sizes=((60, 60)))
    file = models.FileField(_(u"فایل"), upload_to=update_filename, blank=True, null=True)
    position = models.PositiveSmallIntegerField(_(u"Position"), default=999)
    mime_type = models.CharField(max_length=40, blank=True, default="")
    title = models.CharField(max_length=200, blank=True, default="")
    description = models.TextField(blank=True, max_length=200)
    insertion_datetime = models.DateTimeField(_(u"Insertion Date Time"), null=True, auto_now_add=True)

    def __unicode__(self):
        return str(self.title)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = __("فایل های بارگزاری شده")
        verbose_name_plural =  __("فایل های بارگزاری شده")
