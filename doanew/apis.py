from django.conf.urls import  include
from news import apis as news_apis
from django.urls import path

urlpatterns = [
    path(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path("news/", include(news_apis)),
]
